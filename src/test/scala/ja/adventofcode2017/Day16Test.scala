package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day16Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day16.part1(Source.fromResource("day16/test.txt").mkString, 5) == "baedc")
  }

  test("Part 1") {
    assert(Day16.part1(Source.fromResource("day16/input.txt").mkString, 16) == "giadhmkpcnbfjelo")
  }

  test("Part 2 test") {
    assert(Day16.part2(Source.fromResource("day16/test.txt").mkString, 5, 2) == "ceadb")
  }

  test("Part 2 test 2") {
    assert(Day16.part2(Source.fromResource("day16/test.txt").mkString, 5, 22) == "ceadb")
  }

  test("Part 2") {
    assert(Day16.part2(Source.fromResource("day16/input.txt").mkString, 16, 1000_000_000) == "njfgilbkcoemhpad")
  }

}
