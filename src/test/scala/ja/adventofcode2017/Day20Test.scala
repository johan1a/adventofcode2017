package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day20Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day20.part1(Source.fromResource("day20/test.txt").mkString) == 0)
  }

  test("Part 1") {
    assert(Day20.part1(Source.fromResource("day20/input.txt").mkString) == 125)
  }

  test("Part 2 test") {
    assert(Day20.part2(Source.fromResource("day20/test.txt").mkString) == 2)
  }

  test("Part 2 test 1") {
    assert(Day20.part2(Source.fromResource("day20/test1.txt").mkString) == 1)
  }

  test("Part 2") {
    assert(Day20.part2(Source.fromResource("day20/input.txt").mkString) == 461)
  }
}
