package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day14Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day14.part1("flqrgnkx") == 8108)
  }

  test("Part 1") {
    assert(Day14.part1("xlqgujun") == 8204)
  }

  test("Part 2 test") {
    assert(Day14.part2("flqrgnkx") == 1242)
  }

  test("Part 2") {
    assert(Day14.part2("xlqgujun") == 1089)
  }
}
