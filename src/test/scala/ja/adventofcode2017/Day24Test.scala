package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day24Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day24.part1(Source.fromResource("day24/test.txt").mkString) == 31)
  }

  test("Part 1") {
    assert(Day24.part1(Source.fromResource("day24/input.txt").mkString) == 1940)
  }

  test("Part 2 test") {
    assert(Day24.part2(Source.fromResource("day24/test.txt").mkString) == 19)
  }

  test("Part 2") {
    assert(Day24.part2(Source.fromResource("day24/input.txt").mkString) == 1928)
  }

}
