package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day18Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day18.part1(Source.fromResource("day18/test.txt").mkString) == 4)
  }

  test("Part 1") {
    assert(Day18.part1(Source.fromResource("day18/input.txt").mkString) == 1187)
  }

  test("Part 2 test") {
    assert(Day18.part2(Source.fromResource("day18/test1.txt").mkString) == 3)
  }

  test("Part 2") {
    assert(Day18.part2(Source.fromResource("day18/input.txt").mkString) == 5969)
  }

}
