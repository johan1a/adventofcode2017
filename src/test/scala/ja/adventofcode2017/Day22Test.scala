package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day22Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day22.part1(Source.fromResource("day22/test.txt").mkString,  70) == 41)
  }

  test("Part 1 test 1") {
    assert(Day22.part1(Source.fromResource("day22/test.txt").mkString,  10000) == 5587)
  }

  test("Part 1") {
    assert(Day22.part1(Source.fromResource("day22/input.txt").mkString,  10000) == 5460)
  }

  test("Part 2 test") {
    assert(Day22.part2(Source.fromResource("day22/test.txt").mkString,  100) == 26)
  }

  test("Part 2 test 2") {
    assert(Day22.part2(Source.fromResource("day22/test.txt").mkString,  10000000) == 2511944)
  }

  test("Part 2") {
    assert(Day22.part2(Source.fromResource("day22/input.txt").mkString,  10000000) == 2511702)
  }
}
