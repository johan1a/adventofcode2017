package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day21Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day21.part1(Source.fromResource("day21/test.txt").mkString, 2) == 12)
  }

  test("Part 1") {
    assert(Day21.part1(Source.fromResource("day21/input.txt").mkString, 5) == 150)
  }

  test("Part 2") {
    assert(Day21.part2(Source.fromResource("day21/input.txt").mkString) == 0)
  }
}
