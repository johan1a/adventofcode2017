package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day17Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day17.part1(3) == 638)
  }

  test("Part 1") {
    assert(Day17.part1(312) == 772)
  }

  test("Part 2") {
    assert(Day17.part2(312) == 42729050)
  }

}
