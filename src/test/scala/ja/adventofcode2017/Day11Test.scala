package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day11Test extends AnyFunSuite {

  test("Part 1 test 1") {
    assert(Day11.part1("ne,ne,ne") == 3)
  }

  test("Part 1 test 2") {
    assert(Day11.part1("ne,ne,sw,sw") == 0)
  }

  test("Part 1 test 3") {
    assert(Day11.part1("ne,ne,s,s") == 2)
  }

  test("Part 1 test 4") {
    assert(Day11.part1("se,sw,se,sw,sw") == 3)
  }

  test("Part 1 test 5") {
    assert(Day11.part1("se,ne,se,ne,se,nw") == 4)
  }

  test("Part 1 test 6") {
    assert(Day11.part1("se,se,se,sw,sw,sw") == 3)
  }

  test("Part 1 test 7") {
    assert(Day11.part1("nw,sw,nw,sw,s") == 4)
  }

  test("Part 1 test 8") {
    assert(Day11.part1("se,ne,ne,n") == 3)
  }

  test("Part 1 test 9") {
    assert(Day11.part1("ne,se,ne,se,ne,se,se,s,n,s,n,n,s,s,s,s,sw,sw,nw,nw,nw,s,s,nw") == 5)
  }

  test("Part 1") {
    assert(Day11.part1(Source.fromResource("day11/input.txt").mkString) == 796)
  }

  test("Part 2") {
    assert(Day11.part2(Source.fromResource("day11/input.txt").mkString) == 1585)
  }
}
