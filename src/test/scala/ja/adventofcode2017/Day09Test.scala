package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day09Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day09.part1("{}") == 1)
  }

  test("part 1 test 2") {
    assert(Day09.part1("{{{}}}") == 6)
  }

  test("part 1 test 3") {
    assert(Day09.part1("{{},{}}") == 5)
  }

  test("part 1 test 4") {
    assert(Day09.part1("{{{},{},{{}}}}") == 16)
  }

  test("part 1 test 5") {
    assert(Day09.part1("{<a>,<a>,<a>,<a>}") == 1)
  }

  test("part 1 test 6") {
    assert(Day09.part1("{{<ab>},{<ab>},{<ab>},{<ab>}}") == 9)
  }

  test("part 1 test 7") {
    assert(Day09.part1("{{<!!>},{<!!>},{<!!>},{<!!>}}") == 9)
  }

  test("part 1 test 8") {
    assert(Day09.part1("{{<a!>},{<a!>},{<a!>},{<ab>}}") == 3)
  }

  test("part 1") {
    assert(Day09.part1(Source.fromResource("day09/input.txt").mkString) == 9662)
  }

  test("part 2 test 1") {
    assert(Day09.part2("<{o\"i!a,<{i<a>") == 10)
  }

  test("part 2") {
    assert(Day09.part2(Source.fromResource("day09/input.txt").mkString) == 4903)
  }

}
