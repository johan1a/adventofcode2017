package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day02Test extends AnyFunSuite {
  test("1122") {
    assert(Day02.part1("9 5 7") == 4)
  }

  test("part1") {
    assert(Day02.part1(getInput()) == 32020)
  }

  test("part2 small") {
    assert(Day02.part2("2 3 4") == 2)
  }

  test("part2") {
    assert(Day02.part2(getInput()) == 236)
  }

  def getInput(): String = {
    Source.fromResource("day02/input.txt").mkString
  }
}
