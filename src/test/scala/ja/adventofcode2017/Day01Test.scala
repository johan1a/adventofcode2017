package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day01Test extends AnyFunSuite {
  test("part 1 test 1122") {
    assert(Day01.part1("1122") == 3)
  }

  test("part 1") {
    assert(Day01.part1(getInput()) == 1203)
  }

  test("part 2 1212") {
    assert(Day01.part2("1212") == 6)
  }

  test("part 2") {
    assert(Day01.part2(getInput()) == 1146)
  }

  def getInput(): String = {
    Source.fromResource("day01/input.txt").mkString
  }
}
