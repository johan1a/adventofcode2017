package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day08Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day08.part1(Source.fromResource("day08/testinput.txt").mkString) == 1)
  }

  test("part 1") {
    assert(Day08.part1(Source.fromResource("day08/input.txt").mkString) == 4647)
  }

  test("part 2") {
    assert(Day08.part2(Source.fromResource("day08/input.txt").mkString) == 5590)
  }
}
