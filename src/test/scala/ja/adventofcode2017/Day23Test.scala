package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day23Test extends AnyFunSuite {

  test("Part 1") {
    assert(Day23.part1(Source.fromResource("day23/input.txt").mkString) == 3025)
  }

  test("Part 2") {
    assert(Day23.part2(Source.fromResource("day23/input.txt").mkString) == 0)
  }

}
