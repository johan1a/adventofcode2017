package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day04Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day04.part1("art ahey\nwoa bala wao hey") == 2)
  }

  test("part 1") {
    assert(Day04.part1(getInput()) == 477)
  }

  test("part 2 test 1") {
    assert(Day04.part2("abe eba\nka aak") == 1)
  }

  test("part 2") {
    assert(Day04.part2(getInput()) == 167)
  }

  def getInput(): String = {
    Source.fromResource("day04/input.txt").mkString
  }
}
