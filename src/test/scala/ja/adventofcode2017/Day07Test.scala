package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day07Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day07.part1(Source.fromResource("day07/testinput.txt").mkString) == "tknk")
  }

  test("part 1") {
    assert(Day07.part1(Source.fromResource("day07/input.txt").mkString) == "xegshds")
  }

  test("part 2 test") {
    assert(Day07.part2(Source.fromResource("day07/testinput.txt").mkString) == 60)
  }

  test("part 2 test 2") {
    assert(Day07.part2(Source.fromResource("day07/testinput2.txt").mkString) == 5)
  }

  test("part 2 test 3") {
    assert(Day07.part2(Source.fromResource("day07/testinput3.txt").mkString) == 6)
  }

  test("part 2") {
    assert(Day07.part2(Source.fromResource("day07/input.txt").mkString) == 299)
  }

}
