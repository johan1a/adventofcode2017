package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day13Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day13.part1(Source.fromResource("day13/test.txt").mkString) == 24)
  }

  test("Part 1") {
    assert(Day13.part1(Source.fromResource("day13/input.txt").mkString) == 1928)
  }

  test("Part 2 test") {
    assert(Day13.part2(Source.fromResource("day13/test.txt").mkString) == 10)
  }

  test("Part 2") {
    assert(Day13.part2(Source.fromResource("day13/input.txt").mkString) == 3830344)
  }
}
