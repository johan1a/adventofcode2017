package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day25Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day25.part1(Source.fromResource("day25/test.txt").mkString) == 3)
  }

  test("Part 1") {
    assert(Day25.part1(Source.fromResource("day25/input.txt").mkString) == 4225)
  }
}
