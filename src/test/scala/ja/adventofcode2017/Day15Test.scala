package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day15Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(Day15.part1(65, 8921) == 588)
  }

  test("Part 1") {
    assert(Day15.part1(516, 190) == 597)
  }

  test("Part 2 test") {
    assert(Day15.part2(65, 8921) == 309)
  }

  test("Part 2") {
    assert(Day15.part2(516, 190) == 303)
  }
}
