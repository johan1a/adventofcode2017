package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day12Test extends AnyFunSuite {

  test("Part 1") {
    assert(Day12.part1(Source.fromResource("day12/input.txt").mkString) == 239)
  }

  test("Part 2") {
    assert(Day12.part2(Source.fromResource("day12/input.txt").mkString) == 215)
  }
}
