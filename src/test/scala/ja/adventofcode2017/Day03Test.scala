package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day03Test extends AnyFunSuite {
  test("test 1") {
    assert(Day03.part1(1) == 0)
  }

  test("test 3") {
    assert(Day03.part1(3) == 2)
  }

  test("test 23") {
    assert(Day03.part1(23) == 2)
  }

  test("part 1") {
    assert(Day03.part1(289326) == 419)
  }

  test("part 2 test 1") {
    assert(Day03.part2(2) == 4)
  }

  test("part 2") {
    assert(Day03.part2(289326) == 295229)
  }
}
