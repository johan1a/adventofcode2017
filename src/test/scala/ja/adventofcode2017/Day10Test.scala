package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day10Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day10.part1(5, "3, 4, 1, 5") == 12)
  }

  test("part 1") {
    assert(Day10.part1(256, Source.fromResource("day10/input.txt").mkString) == 2928)
  }

  test("part 2 test 1") {
    val input = Seq(65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22)
    assert(Day10.makeDenseHash(input ++ input) == "4040")
  }

  test("part 2 test 2") {
    assert(Day10.part2("") == "a2582a3a0e66e6e86e3812dcb672a272")
  }

  test("part 2 test 3") {
    assert(Day10.part2("AoC 2017") == "33efeb34ea91902bb2f59c9920caa6cd")
  }

  test("part 2") {
    assert(Day10.part2(Source.fromResource("day10/input.txt").mkString) == "0c2f794b2eb555f7830766bf8fb65a16")
  }

}
