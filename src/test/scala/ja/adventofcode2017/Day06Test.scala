package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day06Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day06.part1("0 2 7 0") == 5)
  }

  test("part 1") {
    assert(Day06.part1(Source.fromResource("day06/input.txt").mkString) == 4074)
  }

  test("part 2 test 1") {
    assert(Day06.part2("2 4 1 2") == 4)
  }

  test("part 2") {
    assert(Day06.part2(Source.fromResource("day06/input.txt").mkString) == 2793)
  }

}
