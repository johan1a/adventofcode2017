package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day05Test extends AnyFunSuite {
  test("part 1 test 1") {
    assert(Day05.part1("1") == 1)
  }

  test("part 1 test 2") {
    assert(Day05.part1("0\n3\n0\n1\n-3") == 5)
  }

  test("part 1") {
    assert(Day05.part1(Source.fromResource("day05/input.txt").mkString) == 343467)
  }

  test("part 2 test 1") {
    assert(Day05.part2("0\n3\n0\n1\n-3") == 10)
  }

  test("part 2") {
    assert(Day05.part2(Source.fromResource("day05/input.txt").mkString) == 24774780)
  }

}
