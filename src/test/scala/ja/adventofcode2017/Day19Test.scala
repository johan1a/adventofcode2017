package ja.adventofcode2017

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class Day19Test extends AnyFunSuite {

  test("Part 1 test") {
    assert(
      Day19.part1(Source.fromResource("day19/test.txt").mkString) == "ABCDEF"
    )
  }

  test("Part 1") {
    assert(
      Day19
        .part1(Source.fromResource("day19/input.txt").mkString) == "ITSZCJNMUO"
    )
  }

  test("Part 2 test") {
    assert(Day19.part2(Source.fromResource("day19/test.txt").mkString) == 38)
  }

  test("Part 2") {
    assert(
      Day19.part2(Source.fromResource("day19/input.txt").mkString) == 17420
    )
  }
}
