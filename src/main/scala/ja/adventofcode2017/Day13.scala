package ja.adventofcode2017

import scala.collection.mutable

object Day13 {

  def part1(input: String): Int = {
    var (layers, scanners, end) = parse(input)
    var severity = 0
    var packet = 0
    while (packet <= end) {
      if (layers.contains(packet) && scanners(packet)._1 == 0) {
        severity += packet * scanners(packet)._2
      }
      packet += 1
      scanners = updateScanners(scanners)
    }

    severity
  }

  def part2(input: String): Int = {
    var (layers, startingScanners, end) = parse(input)
    var reachedBeginning = false
    var ms = 0
    while (!reachedBeginning) {
      if (ms % 100000 == 0) { println(ms) }
      var succeeded = run(layers, startingScanners, end)
      if (succeeded) {
        return ms
      }
      ms += 1
      startingScanners = updateScanners(startingScanners)
      reachedBeginning = hasReachedBeginning(startingScanners)
    }
    -1
  }

  private def run(
      layers: mutable.Map[Int, Int],
      startingScanners: Map[Int, (Int, Int, Int)],
      end: Int
  ): Boolean = {

    var scanners = startingScanners
    var packet = 0
    var failed = false
    while (!failed && packet <= end) {
      if (layers.contains(packet) && scanners(packet)._1 == 0) {
        return false
      }
      packet += 1
      scanners = updateScanners(scanners)
    }
    true
  }

  private def hasReachedBeginning(
      scanners: Map[Int, (Int, Int, Int)]
  ): Boolean = {
    scanners.keys.foreach { scanner =>
      if (scanners(scanner)._1 != 0) {
        return false
      }
    }
    println("has reached beginning")
    true
  }

  def updateScanners(scanners: Map[Int, (Int, Int, Int)]) = {
    scanners.keys.map { scanner =>
      val state = scanners(scanner)
      val pos = state._1
      val width = state._2
      var direction = state._3
      if (pos == width - 1) {
        direction = -1
      } else if (pos == 0) {
        direction = 1
      }
      scanner -> (
        pos + direction,
        width,
        direction
      )
    }.toMap
  }

  private def parse(input: String) = {
    val lines = input.split("\n").toSeq
    val layers = mutable.Map[Int, Int]()
    var end = -1
    lines.foreach { line =>
      val split = line.split(":").map(_.trim().toInt)
      layers(split(0)) = split(1)
      end = split(0)
    }
    val scanners: Map[Int, (Int, Int, Int)] = layers.keys.map { layer =>
      layer -> (0, layers(layer), 1)
    }.toMap
    (layers, scanners, end)
  }

}
