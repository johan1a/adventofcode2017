package ja.adventofcode2017

object Day15 {

  val aFactor = 16807
  val bFactor = 48271
  val div = 2147483647

  def part1(aStart: Long, bStart: Long): Int = {
    var matches = 0

    var a = aStart
    var b = bStart
    (0 until 40_000_000).foreach { _ =>
      a = a * aFactor % div
      b = b * bFactor % div
      val aLowest = a & 0xffff
      val bLowest = b & 0xffff
      if (aLowest == bLowest) {
        matches += 1
      }
    }

    matches
  }

  def part2(aStart: Long, bStart: Long): Int = {
    var matches = 0

    var a = aStart
    var b = bStart
    (0 until 5_000_000).foreach { _ =>
      a = a * aFactor % div
      while (a % 4 != 0) {
        a = a * aFactor % div
      }
      b = b * bFactor % div
      while (b % 8 != 0) {
        b = b * bFactor % div
      }
      val aLowest = a & 0xffff
      val bLowest = b & 0xffff
      if (aLowest == bLowest) {
        matches += 1
      }
    }

    matches
  }

}
