package ja.adventofcode2017

import scala.collection.mutable

object Day03 {

  /**
   * 17  16  15  14  13
   * 18   5   4   3  12
   * 19   6   1   2  11
   * 20   7   8   9  10
   * 21  22  23---> ...
   * 1, 8, 16, 24
   */

  def part1(target: Int): Int = {
    var n = 1
    var pos = (0, 0)
    var maxStepsInCurrentDirection = 1
    var direction = 'R'

    while (n < target) {
      0.until(2).foreach(_ => {
        var stepsInCurrentDirection = 0
        while (n < target && stepsInCurrentDirection < maxStepsInCurrentDirection) {
          pos = move(pos, direction)
          stepsInCurrentDirection += 1
          n += 1
        }
        direction = turnLeft(direction)
      })
      maxStepsInCurrentDirection += 1
    }

    manhattanDistance(pos)
  }

  private def turnLeft(dir: Char) = {
    dir match {
      case 'U' => 'L'
      case 'D' => 'R'
      case 'L' => 'D'
      case 'R' => 'U'
    }
  }

  def move(pos: (Int, Int), dir: Char): (Int, Int) = {
    val step = 1
    dir match {
      case 'U' => (pos._1 - step, pos._2)
      case 'D' => (pos._1 + step, pos._2)
      case 'L' => (pos._1, pos._2 - step)
      case 'R' => (pos._1, pos._2 + step)
    }
  }

  def part2(target: Int): Int = {
    var sum = 1
    var pos = (0, 0)
    var maxStepsInCurrentDirection = 1
    var direction = 'R'

    val saved = mutable.Map[(Int, Int), Int]().withDefaultValue(0)
    saved(pos) = 1

    while (sum <= target) {
      0.until(2).foreach(_ => {
        var stepsInCurrentDirection = 0
        while (sum <= target && stepsInCurrentDirection < maxStepsInCurrentDirection) {
          pos = move(pos, direction)
          stepsInCurrentDirection += 1

          saved(pos) = calculateNewSum(pos, saved)
          sum = saved(pos)
        }
        direction = turnLeft(direction)
      })
      maxStepsInCurrentDirection += 1
    }

    sum
  }

  def calculateNewSum(pos: (Int, Int), saved: mutable.Map[(Int, Int), Int]): Int = {
    var sum = 0
    (pos._1 - 1).to(pos._1 + 1).foreach(y =>
      (pos._2 - 1).to(pos._2 + 1).foreach(x =>
        if (y != pos._1 || x != pos._2) {
          sum += saved((y, x))
        }
      )
    )
    sum
  }

  def manhattanDistance(pos: (Int, Int)): Int = {
    Math.abs(pos._1) + Math.abs(pos._2)
  }

}
