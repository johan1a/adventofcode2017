package ja.adventofcode2017

import scala.collection.mutable

object Day08 {

  type Register = String

  class Condition()

  case class Greater(register: Register, amount: Int) extends Condition

  case class GreaterEq(register: Register, amount: Int) extends Condition

  case class Lesser(register: Register, amount: Int) extends Condition

  case class LesserEq(register: Register, amount: Int) extends Condition

  case class Equal(register: Register, amount: Int) extends Condition

  case class NotEqual(register: Register, amount: Int) extends Condition

  class Instruction()

  case class Inc(target: Register, amount: Int, condition: Condition) extends Instruction

  case class Dec(target: Register, amount: Int, condition: Condition) extends Instruction

  private val regex = "([a-z]+) ([a-z]+) ([-]?[0-9]+) if ([a-z]+) ([!=<>]+) ([-]?[0-9]+)".r

  private var registers = mutable.Map[Register, Int]().withDefaultValue(0)

  def parseInput(input: String): Seq[Instruction] = {
    regex.findAllMatchIn(input)
      .map(patternMatch => {
        val groups = patternMatch.subgroups
        val target = groups.head.trim
        val instructionType = groups(1).trim
        val amount = groups(2).trim.toInt

        val condTarget = groups(3).trim
        val operator = groups(4).trim
        val condAmount = groups(5).trim.toInt

        val condition = operator match {
          case ">" => Greater(condTarget, condAmount)
          case ">=" => GreaterEq(condTarget, condAmount)
          case "<" => Lesser(condTarget, condAmount)
          case "<=" => LesserEq(condTarget, condAmount)
          case "==" => Equal(condTarget, condAmount)
          case "!=" => NotEqual(condTarget, condAmount)
        }

        instructionType match {
          case "inc" => Inc(target, amount, condition)
          case "dec" => Dec(target, amount, condition)
        }
      }).toList
  }

  def part1(input: String): Int = {
    registers = mutable.Map[Register, Int]().withDefaultValue(0)
    val instructions = parseInput(input)
    instructions.foreach(execute)
    registers.values.max
  }

  def part2(input: String): Int = {
    registers = mutable.Map[Register, Int]().withDefaultValue(0)
    val instructions = parseInput(input)
    var max = 0
    instructions.foreach { instruction =>
      execute(instruction)
      max = Math.max(max, registers.values.max)
    }
    max
  }

  def evaluate(condition: Condition): Boolean = condition match {
    case Equal(register, amount) => registers(register) == amount
    case NotEqual(register, amount) => registers(register) != amount
    case Lesser(register, amount) => registers(register) < amount
    case LesserEq(register, amount) => registers(register) <= amount
    case Greater(register, amount) => registers(register) > amount
    case GreaterEq(register, amount) => registers(register) >= amount
  }

  def execute(instruction: Instruction): Unit = instruction match {
    case Inc(target, amount, condition) =>
      if (evaluate(condition)) {
        registers(target) += amount
      }
    case Dec(target, amount, condition) =>
      if (evaluate(condition)) {
        registers(target) -= amount
      }
  }

}

