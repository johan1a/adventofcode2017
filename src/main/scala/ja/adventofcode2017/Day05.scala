package ja.adventofcode2017

object Day05 {
  def part1(input: String): Int = {
    val numbers = input.split("\\n").map(_.toInt)
    var nbrSteps = 0
    var i = 0
    while (i >= 0 && i < numbers.length) {
      val jump = numbers(i)
      val next = i + jump
      numbers(i) += 1
      nbrSteps += 1
      i = next
    }

    nbrSteps
  }

  def part2(input: String): Int = {
    val numbers = input.split("\\n").map(_.toInt)
    var nbrSteps = 0
    var i = 0
    while (i >= 0 && i < numbers.length) {
      val jump = numbers(i)
      val next = i + jump
      if(jump >= 3) {
        numbers(i) += -1
      } else {
        numbers(i) += 1
      }
      nbrSteps += 1
      i = next
    }

    nbrSteps
  }

}
