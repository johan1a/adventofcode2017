package ja.adventofcode2017

object Day02 {
  def part1(input: String): Int = {
    val lines = input.split("\n")
    lines.map(line => {
      val nums = line.split("\\s").map(Integer.parseInt)
      nums.max - nums.min
    }).sum
  }

  def part2(input: String): Int = {
    val lines = input.split("\n")

    var sum = 0
    lines.foreach(line => {
      val nums = line.split("\\s").map(Integer.parseInt).toArray
      nums.indices.foreach { i =>
        (i + 1).until(nums.length).foreach { j =>
          val a = nums(i)
          val b = nums(j)
          if (a % b == 0) {
            sum += a / b
          } else if (b % a == 0) {
            sum += b / a
          }
        }
      }
    })
    sum
  }

}
