package ja.adventofcode2017


import scala.collection.mutable

object Day06 {

  def part1(input: String): Int = {
    val banks: Array[Int] = input.split("\\s").map(_.toInt)
    var configuration = banks.mkString
    var seen = Set[String]()
    var nbrRedistributions = 0
    while (!seen.contains(configuration)) {
      seen = seen + configuration
      var i = maxIndex(banks)
      var buffer = banks(i)
      banks(i) -= buffer
      while (buffer > 0) {
        i = (i + 1) % banks.length
        banks(i) += 1
        buffer -= 1
      }
      nbrRedistributions += 1
      configuration = banks.mkString
    }

    nbrRedistributions
  }


  def part2(input: String): Int = {
    val banks: Array[Int] = input.split("\\s").map(_.toInt)
    var configuration = banks.mkString
    var seenAt = mutable.Map[String, Int]()
    var seen = Set[String]()
    var nbrRedistributions = 0
    while (!seen.contains(configuration)) {
      seen = seen + configuration
      seenAt(configuration) = nbrRedistributions
      var i = maxIndex(banks)
      var buffer = banks(i)
      banks(i) -= buffer
      while (buffer > 0) {
        i = (i + 1) % banks.length
        banks(i) += 1
        buffer -= 1
      }
      nbrRedistributions += 1
      configuration = banks.mkString
    }
    nbrRedistributions - seenAt(configuration)
  }

  def maxIndex(banks: Array[Int]): Int = {
    var i = 0
    var max = banks.head
    banks.zipWithIndex.foreach { case (n: Int, index: Int) =>
      if (n > max) {
        i = index
        max = n
      }
    }
    i
  }


}
