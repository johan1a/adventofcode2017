package ja.adventofcode2017

import scala.collection.mutable

object Day18 {

  def part1(input: String): Long = {
    val instructions = parse(input)
    execute(instructions)
  }

  def part2(input: String): Long = {
    val instructions = parse(input)
    var registers0 = Map[String, Long]("p" -> 0).withDefaultValue(0L)
    var registers1 = Map[String, Long]("p" -> 1).withDefaultValue(0L)
    var i0 = 0
    var i1 = 0
    var sent0 = Seq[Long]()
    var sent1 = Seq[Long]()
    var received0 = Seq[Long]()
    var received1 = Seq[Long]()

    var nbrSent1 = 0
    var bothTerminated = false

    while (!bothTerminated) {
      execute2(registers0, instructions, i0, sent0, received0) match {
        case (i, registers, sent, received) =>
          i0 = i
          registers0 = registers
          sent0 = sent
          received0 = received
      }
      received1 ++= sent0
      sent0 = Seq()

      execute2(registers1, instructions, i1, sent1, received1) match {
        case (i, registers, sent, received) =>
          i1 = i
          registers1 = registers
          sent1 = sent
          received1 = received
      }
      received0 ++= sent1
      nbrSent1 += sent1.size
      sent1 = Seq()

      if (
        ((i0 <= 0 || i0 >= instructions.size || received0.isEmpty) &&
        (i1 <= 0 || i1 >= instructions.size || received1.isEmpty))
      ) {
        bothTerminated = true
      }
    }

    nbrSent1
  }

  private def execute(instructions: Seq[Seq[String]]): Long = {
    var registers = Map[String, Long]().withDefaultValue(0L)
    var playedSounds = Seq[Long]()
    var recoveredSounds = Seq[Long]()

    var i = 0L
    while (i >= 0 && i < instructions.size) {
      val instruction = instructions(i.toInt)
      instruction match {
        case Seq("snd", a) =>
          playedSounds = getValue(registers, a) +: playedSounds
        case Seq("set", a, b) =>
          registers = registers.updated(a, getValue(registers, b))
        case Seq("add", a, b) =>
          registers =
            registers.updated(a, registers(a) + getValue(registers, b))
        case Seq("mul", a, b) =>
          registers =
            registers.updated(a, registers(a) * getValue(registers, b))
        case Seq("mod", a, b) =>
          registers =
            registers.updated(a, registers(a) % getValue(registers, b))
        case Seq("rcv", a) =>
          if (getValue(registers, a) != 0) {
            recoveredSounds = playedSounds.head +: recoveredSounds
            return playedSounds.head
          }
        case Seq("jgz", a, b) =>
          if (getValue(registers, a) > 0) {
            i += getValue(registers, b)
            i -= 1
          }
        case other =>
          println("error", other)
          return -1
      }
      i += 1
    }

    recoveredSounds.last
  }

  private def execute2(
      registers0: Map[String, Long],
      instructions: Seq[Seq[String]],
      i0: Int,
      sent0: Seq[Long],
      received0: Seq[Long]
  ): (Int, Map[String, Long], Seq[Long], Seq[Long]) = {
    var registers = registers0
    var sent = sent0
    var received = received0

    var i = i0
    while (i >= 0 && i < instructions.size) {
      val instruction = instructions(i.toInt)
      instruction match {
        case Seq("snd", a) =>
          sent = sent :+ getValue(registers, a)
        case Seq("set", a, b) =>
          registers = registers.updated(a, getValue(registers, b))
        case Seq("add", a, b) =>
          registers =
            registers.updated(a, registers(a) + getValue(registers, b))
        case Seq("mul", a, b) =>
          registers =
            registers.updated(a, registers(a) * getValue(registers, b))
        case Seq("mod", a, b) =>
          registers =
            registers.updated(a, registers(a) % getValue(registers, b))
        case Seq("rcv", a) =>
          if (received.nonEmpty) {
            val next = received.head
            received = received.drop(1)
            registers = registers.updated(a, next)
          } else {
            return (i, registers, sent, received)
          }
        case Seq("jgz", a, b) =>
          if (getValue(registers, a) > 0) {
            i += getValue(registers, b).toInt
            i -= 1
          }
        case other =>
          println("error", other)
      }
      i += 1
    }

    (i, registers, sent, received)
  }

  private def getValue(registers: Map[String, Long], x: String): Long = {
    if (x >= "a" && x <= "z") {
      registers(x)
    } else {
      x.toInt
    }
  }

  private def parse(input: String) = {
    input
      .split("\n")
      .map { line =>
        val split = line.trim().split(" ")
        split.toSeq
      }
      .toSeq
  }

}
