package ja.adventofcode2017

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Day21 {

  def part1(input: String, n: Int = 5): Int = {
    var rules = parse(input)
    var pattern = ".#./..#/###"
    (0 until n).foreach { i =>
      var size = 3
      val grid = pattern.split("/")
      if (grid.size % 2 == 0) {
        size = 2
      }
      val nbrPerRow = grid.size / size

      val divided = divide(pattern, size)

      val transformed = divided.map(p => transform(rules, p)).toArray

      pattern = combine(transformed, nbrPerRow)
    }
    pattern.count(_ == '#')
  }

  def part2(input: String): Int = {
    part1(input, 18)
  }

  private def transform(rules: Map[String, String], pattern: String) = {
    if (rules.contains(pattern)) {
      rules(pattern)
    } else {
      throw new Exception("")
    }
  }

  private def combine(patterns: Array[String], nbrPerRow: Int): String = {
    val grids = patterns.map(_.split("/"))
    val smallGridSize = grids(0).size
    val result = Array.fill(nbrPerRow * smallGridSize)(
      Array.fill(nbrPerRow * smallGridSize)('.')
    )
    (0 until nbrPerRow).foreach { row =>
      (0 until nbrPerRow).foreach { col =>
        (0 until smallGridSize).foreach { i =>
          (0 until smallGridSize).foreach { j =>
            val grid = grids(row * nbrPerRow + col)
            val y = row * smallGridSize + i
            val x = col * smallGridSize + j
            result(y)(x) = grid(i)(j)
          }
        }
      }
    }

    result.map(_.mkString).mkString("/")
  }

  private def divide(pattern: String, n: Int) = {
    val grid = pattern.split("/")
    var squares = Seq[String]()
    var y = 0
    while (y < grid.size) {
      var x = 0
      while (x < grid.size) {
        val square = (0 until n).map { i =>
          (0 until n).map { j =>
            grid(y + i)(x + j)
          }
        }
        squares = squares.appended(square.map(_.mkString).mkString("/"))
        x += n
      }
      y += n
    }
    squares
  }

  private def parse(input: String) = {
    var rules = Map[String, String]()
    input.split("\n").map(_.trim()).map { line =>
      val split = line.split(" => ")
      val from = split.head
      val to = split.last
      rules = rules.updated(from, to)
      (1 until 4).foreach { i =>
        rules = rules.updated(rotate(from, i), to)
      }
      val flipped = flip(from)
      rules = rules.updated(flipped, to)
      (1 until 4).foreach { i =>
        rules = rules.updated(rotate(flipped, i), to)
      }
    }
    rules
  }

  private def flip(pattern: String): String = {
    pattern.split("/").map(_.reverse).mkString("/")
  }

  private def rotate(pattern: String, times: Int): String = {
    var grid = pattern.split("/").map(_.toArray)
    var result: Array[Array[Char]] = null
    (0 until times).foreach { _ =>
      result = Array.fill(grid.size)(Array.fill(grid(0).size)('.'))
      (0 until grid.size).foreach { y =>
        (0 until grid(0).size).foreach { x =>
          val newX = grid.size - 1 - y
          val newY = x
          result(newY)(newX) = grid(y)(x)
        }
      }
      grid = result
    }
    result.map(_.mkString).mkString("/")
  }

  private def printAsGrid(pattern: String) = {
    var grid = pattern.split("/").foreach { line =>
      println(line)
    }
  }

}
