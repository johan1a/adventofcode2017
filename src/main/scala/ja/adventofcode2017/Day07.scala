package ja.adventofcode2017

import scala.collection.mutable

object Day07 {

  private val pattern = "([a-z ]+) \\(([0-9]+)\\)( -> )?([a-z ,]+)?".r
  private var allChildren = mutable.Map[String, List[String]]()
  private var nodeWeights = mutable.Map[String, Int]()
  private var subTreeWeights = mutable.Map[String, Int]()

  def part1(input: String): String = {
    val parents = mutable.Map[String, String]()
    for (patternMatch <- pattern.findAllMatchIn(input)) {
      val name = patternMatch.group(1)
      if (patternMatch.group(4) != null) {
        val children = patternMatch.group(4).split(",")
        children.foreach { child =>
          parents(child.trim) = name
        }
      }
    }
    parents.values.find { parent =>
      !parents.contains(parent)
    }.getOrElse(throw new RuntimeException("Failed"))
  }

  def part2(input: String): Int = {
    allChildren = mutable.Map[String, List[String]]()
    nodeWeights = mutable.Map[String, Int]()
    subTreeWeights = mutable.Map[String, Int]()

    for (patternMatch <- pattern.findAllMatchIn(input)) {
      val name = patternMatch.group(1).trim
      val weight = patternMatch.group(2).trim.toInt
      nodeWeights(name) = weight
      if (patternMatch.group(4) != null) {
        val children = patternMatch.group(4).split(",").map(_.trim)
        if (!children.contains(name)) {
          allChildren(name) = List()
        }
        allChildren(name) = allChildren(name) ++ children
      }
    }

    val root = part1(input)
    calculateWeights(root)
    findNeededNodeSize(root, 0)
  }

  def findNeededNodeSize(node: String, diff: Int): Int = {
    if (allChildren.contains(node)) {
      val subtreeWeights = allChildren(node).map { child => subTreeWeights(child) }
      if (subtreeWeights.max != subtreeWeights.min) {
        val unbalancedTreeNode = allChildren(node)
          .groupBy(subTreeWeights)
          .minBy(group => group._2.size)
          ._2
          .head
        var diff = subtreeWeights.max - subtreeWeights.min
        if (subTreeWeights(unbalancedTreeNode) == subtreeWeights.max) {
          diff = -diff
        }
        findNeededNodeSize(unbalancedTreeNode, diff)
      } else {
        nodeWeights(node) + diff
      }
    } else {
      nodeWeights(node) + diff
    }
  }

  def calculateWeights(node: String): Int = {
    subTreeWeights(node) = nodeWeights(node)
    if (allChildren.contains(node)) {
      subTreeWeights(node) += allChildren(node).map(calculateWeights).sum
    }
    subTreeWeights(node)
  }

}
