package ja.adventofcode2017

import scala.collection.mutable

object Day17 {

  case class Node(var next: Node, value: Int)

  def part1(input: Int): Int = {
    var current: Node = Node(null, 0)
    current.next = current
    var size = 1
    var i = 0
    while (i < 2017) {
      val steps = input % size
      (0 until steps).foreach { _ =>
        current = current.next
      }

      val next = current.next
      current.next = Node(next, i + 1)
      current = current.next
      size += 1
      i += 1
    }

    current.next.value
  }

  def part2(input: Int) = {
    var target = -1
    var size = 1
    var pos = 0
    var i = 0
    while (i < 50_000_000) {
      pos = (pos + input) % size
      size += 1
      pos = (pos + 1) % size
      if (pos == 1) {
        target = i + 1
      }
      i += 1
    }

    target
  }

}
