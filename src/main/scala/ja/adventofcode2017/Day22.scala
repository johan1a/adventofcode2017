package ja.adventofcode2017

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Day22 {

  def part1(input: String, n: Int = 5): Int = {
    var (grid, pos) = parse(input)
    var dir = "up"
    var count = 0
    (0 until n).foreach { _ =>
      grid(pos) match {
        case '#' =>
          dir = turnRight(dir)
          grid(pos) = '.'
        case '.' =>
          dir = turnLeft(dir)
          grid(pos) = '#'
          count += 1
      }
      pos = moveForward(pos, dir)
    }
    count
  }

  def part2(input: String, n: Int = 5): Int = {
    var (grid, pos) = parse(input)
    var dir = "up"
    var count = 0
    (0 until n).foreach { _ =>
      grid(pos) match {
        case '#' =>
          dir = turnRight(dir)
          grid(pos) = 'F'
        case '.' =>
          dir = turnLeft(dir)
          grid(pos) = 'W'
        case 'W' =>
          grid(pos) = '#'
          count += 1
        case 'F' =>
          dir = turnRight(dir)
          dir = turnRight(dir)
          grid(pos) = '.'
      }
      pos = moveForward(pos, dir)
    }
    count
  }

  private def printGrid(
      grid: mutable.Map[(Int, Int), Char],
      pos: (Int, Int)
  ) = {
    val (x0, y0) = pos
    (y0 - 8 until y0 + 8).foreach { y =>
      (x0 - 8 until x0 + 8).foreach { x =>
        if ((x, y) == (x0, y0)) {
          print('X')
        } else {
          print(grid((x, y)))
        }
      }
      println()
    }

  }

  private def moveForward(pos: (Int, Int), dir: String) = {
    val (x, y) = pos
    dir match {
      case "up"    => (x, y - 1)
      case "down"  => (x, y + 1)
      case "left"  => (x - 1, y)
      case "right" => (x + 1, y)
    }
  }

  private def turnRight(dir: String) = {
    dir match {
      case "up"    => "right"
      case "down"  => "left"
      case "left"  => "up"
      case "right" => "down"
    }
  }

  private def turnLeft(dir: String) = {
    dir match {
      case "up"    => "left"
      case "down"  => "right"
      case "left"  => "down"
      case "right" => "up"
    }
  }

  private def parse(input: String) = {
    val split = input.split("\n")
    val grid = mutable.Map[(Int, Int), Char]().withDefaultValue('.')
    (0 until split.size).map { y =>
      (0 until split(0).size).map { x =>
        grid((x, y)) = split(y)(x)
      }
    }
    (grid, (split(0).size / 2, split.size / 2))
  }

}
