package ja.adventofcode2017

import scala.collection.mutable

object Day20 {

  case class Vector(x: Int, y: Int, z: Int)

  case class Particle(id: Int, p: Vector, v: Vector, a: Vector)

  def part1(input: String): Int = {
    var particles = parse(input)
    particles.minBy(p => vectorAbs(p.a)).id
  }

  def part2(input: String): Int = {
    var particles = parse(input)
    var stable = false
    var i = 0
    while (!stable && i < 50) {
      val nextParticles = removeCollisions(particles.map(updateParticle))
      stable = isStable(nextParticles)
      particles = nextParticles
      i += 1
    }
    particles.size
  }

  private def removeCollisions(particles: Seq[Particle]): Seq[Particle] = {
    particles
      .groupBy(_.p)
      .filter { _._2.size == 1 }
      .flatMap { _._2 }
      .toSeq
  }

  private def isStable(
      nextParticles: Seq[Particle]
  ): Boolean = {
    nextParticles.sortBy(distance).sliding(2).foreach {
      case ab =>
        val a = ab.head
        val b = ab.last
        if (a.id != b.id && vectorAbs(b.a) <= (vectorAbs(a.a))) {
          return false
        }
    }
    true
  }

  private def updateParticle(particle: Particle) = {
    val velocity = add(particle.v, particle.a)
    val position = add(particle.p, velocity)
    Particle(particle.id, position, velocity, particle.a)
  }

  private def add(a: Vector, b: Vector) = {
    Vector(a.x + b.x, a.y + b.y, a.z + b.z)
  }

  private def distance(particle: Particle) = {
    vectorAbs(particle.p)
  }

  private def vectorAbs(vector: Vector) = {
    Math.abs(vector.x) + Math.abs(vector.y) + Math.abs(vector.z)
  }

  private def parse(input: String) = {
    val pattern =
      """p=<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>, v=<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>, a=<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>""".r

    input
      .split("\n")
      .zipWithIndex
      .map {
        case (line, i) =>
          val m = pattern.findAllIn(line.trim())
          Particle(
            i,
            Vector(m.group(1).toInt, m.group(2).toInt, m.group(3).toInt),
            Vector(m.group(4).toInt, m.group(5).toInt, m.group(6).toInt),
            Vector(m.group(7).toInt, m.group(8).toInt, m.group(9).toInt)
          )
      }
      .toSeq

  }

}
