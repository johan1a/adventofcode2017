package ja.adventofcode2017

import java.lang.Integer.toHexString

object Day10 {

  def part1(nbrElements: Int, lengthString: String): Int = {
    val lengths = lengthString.split(',').map(_.trim.toInt)
    val list = 0.until(nbrElements).toArray
    executeRound(list, 0, 0, lengths)
    list(0) * list(1)
  }

  def part2(input: String): String = {
    val nbrElements = 256
    val standardSuffix = Seq(17, 31, 73, 47, 23)
    val lengths = input.map { char => char.toInt } ++ standardSuffix
    val list = 0.until(nbrElements).toArray
    var pos = 0
    var skipSize = 0
    0.until(64).foreach { _ =>
      val (newPos, newSkipSize) = executeRound(list, pos, skipSize, lengths)
      pos = newPos
      skipSize = newSkipSize
    }
    makeDenseHash(list)
  }

  def executeRound(list: Array[Int], startingPos: Int, startingskipSize: Int, lengths: Seq[Int]): (Int, Int) = {
    var pos = startingPos
    var skipSize = startingskipSize
    lengths.foreach { length =>
      reverse(list, pos, length)
      pos = (pos + length + skipSize) % list.length
      skipSize += 1
    }
    (pos, skipSize)
  }

  def reverse(list: Array[Int], start: Int, length: Int): Unit = {
    val end = start + length - 1
    0.until(length / 2).foreach { i =>
      val startIndex = (start + i) % list.length
      val endIndex = (end - i) % list.length
      val temp = list(endIndex)
      list(endIndex) = list(startIndex)
      list(startIndex) = temp
    }
  }

  def makeDenseHash(list: Seq[Int]): String = {
    list.grouped(16).map { group =>
      group.foldRight(0)((a, b) => a ^ b)
    }.map(toHexString)
      .map(str =>
        if (str.length == 1) {
          s"0$str"
        } else {
          str
        })
      .mkString
  }

}

