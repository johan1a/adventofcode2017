package ja.adventofcode2017

object Day09 {

  def part1(input: String): Int = {
    runprogram(input)._1
  }

  def part2(input: String): Int = {
    runprogram(input)._2
  }

  def runprogram(input: String): (Int, Int) = {
    var score = 0
    var garbageCount = 0
    var groupDepth = 0
    var isGarbage = false
    var prevWasSkip = false
    0.until(input.length).foreach { i =>
      val char = input.charAt(i)
      if (prevWasSkip) {
        prevWasSkip = false
      } else {
        char match {
          case '{' if !isGarbage => groupDepth += 1
          case '}' if !isGarbage =>
            score += groupDepth
            groupDepth -= 1
          case '<' if !isGarbage => isGarbage = true
          case '>' if isGarbage => isGarbage = false
          case '!' => prevWasSkip = true
          case _ if isGarbage => garbageCount += 1
          case _ =>
        }
      }
    }
    (score, garbageCount)
  }

}

