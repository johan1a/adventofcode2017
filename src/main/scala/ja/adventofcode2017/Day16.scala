package ja.adventofcode2017

import scala.collection.mutable

object Day16 {

  def part1(input: String, n: Int): String = {
    val (instructions, positions, programs) = parse(input, n)
    dance(n, programs, positions, instructions, 1)
  }

  def part2(input: String, n: Int, iterations: Int): String = {
    val (instructions, positions, programs) = parse(input, n)
    dance(n, programs, positions, instructions, iterations)
  }

  type CacheToken = (Int, String)

  private def dance(
      n: Int,
      programs: mutable.ArrayBuffer[String],
      positions: mutable.Map[String, Int],
      instructions: Seq[Any],
      iterations: Int
  ): String = {

    var cache = Map[CacheToken, Int]()
    var reverseCache = Map[Int, CacheToken]()

    var start = 0
    (0 until iterations).foreach { i =>
      val token = (start, toString(start, n, programs))
      if (cache.contains(token)) {
        val firstSeenIndex = cache(token)
        val period = (i - firstSeenIndex)
        val nbrLeft = iterations - i
        val finalIndex = (i + nbrLeft) % period
        return reverseCache(finalIndex)._2
      } else {
        cache = cache + (token -> i)
        reverseCache = reverseCache + (i -> token)
      }
      instructions.foreach {
        case ("s", k: Int) =>
          start -= k
          if (start < 0) {
            start = n + start
          }
          start = start % n
        case ("x", aPos: Int, bPos: Int) =>
          val aPosAdjusted = (start + aPos) % n
          val bPosAdjusted = (start + bPos) % n
          val a = programs(aPosAdjusted)
          val b = programs(bPosAdjusted)
          programs(aPosAdjusted) = b
          programs(bPosAdjusted) = a
          positions(a) = bPosAdjusted
          positions(b) = aPosAdjusted
        case ("p", a: String, b: String) =>
          val aPos = positions(a)
          val bPos = positions(b)
          programs(bPos) = a
          programs(aPos) = b
          positions(a) = bPos
          positions(b) = aPos
      }
    }
    toString(start, n, programs)
  }

  private def toString(
      start: Int,
      n: Int,
      programs: mutable.ArrayBuffer[String]
  ) = {
    (start until (start + n)).map { i =>
      programs(i % n)
    }.mkString
  }

  private def parse(input: String, n: Int) = {
    val instructions = input.trim().split(",").map { s =>
      if (s.startsWith("s")) {
        ("s", s.drop(1).toInt)
      } else if (s.startsWith("x")) {
        val ab = s.drop(1).split("/").map(_.toInt)
        ("x", ab.head, ab.last)
      } else {
        val ab = s.drop(1).split("/")
        ("p", ab.head, ab.last)
      }
    }
    val positions = mutable.Map[String, Int]()
    val programs = mutable.ArrayBuffer[String]()
    ('a'.toInt until 'a'.toInt + n).zipWithIndex.foreach {
      case (k, i) =>
        programs += k.toChar.toString()
        positions(k.toChar.toString) = i
    }
    (instructions, positions, programs)
  }

}
