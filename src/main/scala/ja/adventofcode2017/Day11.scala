package ja.adventofcode2017

object Day11 {

  def part1(input: String): Int = {
    val directions = input.split(",")

    val start = (0, 0, 0)
    var pos = start
    directions.foreach { dir =>
      pos = move(pos, dir)
    }

    distanceBetween(start, pos)
  }

  def part2(input: String): Int = {
    val directions = input.split(",")

    val start = (0, 0, 0)
    var pos = start
    var max = 0
    var maxPos = start
    directions.foreach { dir =>
      pos = move(pos, dir)

      val distance = distanceBetween(start, pos)
      if (distance > max) {
        maxPos = pos
        max = distance
      }
    }
    max
  }

  def move(pos: (Int, Int, Int), dir: String) = {
    var r = pos._1
    var q = pos._2
    var s = pos._3
    dir.trim() match {
      case "n"  => (r - 1, q, s + 1)
      case "s"  => (r + 1, q, s - 1)
      case "ne" => (r - 1, q + 1, s)
      case "se" => (r, q + 1, s - 1)
      case "nw" => (r, q - 1, s + 1)
      case "sw" => (r + 1, q - 1, s)
    }
  }

  def distanceBetween(from: (Int, Int, Int), to: (Int, Int, Int)): Int = {
    (Math.abs(from._1 - to._1) + Math.abs(from._2 - to._2) + Math.abs(
      from._3 - to._3
    )) / 2
  }

}
