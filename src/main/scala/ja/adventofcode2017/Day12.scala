package ja.adventofcode2017

import scala.collection.mutable

object Day12 {

  def part1(input: String): Int = {
    val neighbors = parse(input)
    getGroup(neighbors, 0).size
  }

  def part2(input: String): Int = {
    val neighbors = parse(input)
    var seen = mutable.Set[Int]()
    var nbrGroups = 0
    neighbors.keys.foreach { node =>
      if (!seen.contains(node)) {
        val group = getGroup(neighbors, node)
        seen ++= group
        nbrGroups += 1
      }
    }

    nbrGroups
  }

  private def getGroup(neighbors: mutable.Map[Int, Set[Int]], node: Int) = {
    var found = Set[Int]()
    var toCheck = mutable.Set(node)
    while (toCheck.nonEmpty) {
      val node = toCheck.head
      toCheck.remove(node)
      found = found + node
      neighbors(node).filter(!found.contains(_)).foreach { neighbor =>
        toCheck = toCheck + neighbor
      }
    }
    found
  }

  private def parse(input: String) = {
    val lines = input.split("\n").toSeq
    val neighbors = mutable.Map[Int, Set[Int]]().withDefaultValue(Set())
    lines.map { line =>
      val left, rights = line.trim().split(" <-> ")
      val leftInt = left(0).trim().toInt
      val rightInts = rights(1).split(",").map(_.trim().toInt)
      neighbors(leftInt) ++= rightInts
      rightInts.foreach { neighbor =>
        neighbors(neighbor) += leftInt
      }
    }
    neighbors
  }

}
