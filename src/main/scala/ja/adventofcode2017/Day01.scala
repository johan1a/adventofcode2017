package ja.adventofcode2017

object Day01 {

  def part1(input: String): Int = {
    var sum = 0
    val allDigits: Seq[Int] = input.trim.toSeq.map(_.toString).map(Integer.parseInt)
    allDigits.sliding(2).foreach { (digits: Seq[Int]) =>
      if (digits.head == digits(1)) {
        sum += digits.head
      }
    }
    if (allDigits.size > 1 && allDigits.last == allDigits.head) {
      sum += allDigits.last
    }
    sum
  }

  def part2(input: String): Int = {
    val allDigits = input.trim.toSeq.map(_.toString).map(Integer.parseInt).toArray
    var sum = 0
    allDigits.zipWithIndex.foreach { case (digit, index) =>
      val nextIndex = (index + allDigits.length / 2) % allDigits.length
      if (digit == allDigits(nextIndex)) {
        sum += digit
      }
    }
    sum
  }

}
