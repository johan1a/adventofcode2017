package ja.adventofcode2017

import scala.collection.mutable

object Day04 {
  def part1(input: String): Int = {
    input.split("\n").map { line =>
      val words = line.split("\\s")
      if (words.toSet.size == words.length) {
        1
      } else {
        0
      }
    }.sum
  }

  def part2(input: String): Int = {
    input.split("\n").map { line =>
      val words = line.split("\\s")
      val seen = mutable.Set[String]()
      words.foreach(word =>
        seen.add(word.sorted)
      )
      if (seen.size == words.length) {
        1
      } else {
        0
      }
    }.sum
  }

}
