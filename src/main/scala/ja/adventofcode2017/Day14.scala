package ja.adventofcode2017

import scala.collection.mutable

object Day14 {

  def part1(key: String): Int = {
    val grid = createGrid(key)
    var total = 0
    (0 until 128).map { i =>
      (0 until 128).map { j =>
        if (grid(i)(j)) {
          total += 1
        }
      }
    }

    total
  }

  def part2(key: String): Int = {
    val grid = createGrid(key)
    val active = mutable.Queue[(Int, Int)]()
    (0 until 128).map { i =>
      (0 until 128).map { j =>
        if (grid(i)(j)) {
          active.enqueue((i, j))
        }
      }
    }

    var toCheck = Set[(Int, Int)]()
    var visited = Set[(Int, Int)]()
    var groups = 0
    while (active.nonEmpty) {
      var tentative = active.dequeue()
      while (active.nonEmpty && visited.contains(tentative)) {
        tentative = active.dequeue()
      }
      if (active.nonEmpty) {
        toCheck += tentative
        while (toCheck.nonEmpty) {
          val pos = toCheck.head
          toCheck = toCheck - pos
          visited += pos
          Set((1, 0), (-1, 0), (0, 1), (0, -1)).map {
            case (i, j) =>
              val newPos = (pos._1 + i, pos._2 + j)
              if (
                !visited.contains(newPos) &&
                newPos._1 >= 0 && newPos._1 <= 127 && newPos._2 >= 0 && newPos._2 <= 127
                && grid(pos._1)(pos._2)
              ) {
                toCheck += ((pos._1 + i, pos._2 + j))
              }
          }
        }
        groups += 1
      }
    }

    groups
  }

  private def createGrid(key: String) = {
    val grid = mutable
      .Map[Int, mutable.Map[Int, Boolean]]()
    (0 until 128).map { i =>
      grid(i) = mutable.Map[Int, Boolean]()
      val hash = Day10.part2(s"$key-$i")
      hash.zipWithIndex.foreach {
        case (c, j) =>
          var bits = Integer.parseInt(c.toString(), 16).toBinaryString
          while (bits.size < 4) {
            bits = "0" + bits
          }
          bits.zipWithIndex.foreach {
            case (b: Char, k: Int) =>
              if (b == '1') {
                grid(i)(4 * j + k) = true
              } else {
                grid(i)(4 * j + k) = false
              }
          }
      }
    }
    grid
  }
}
