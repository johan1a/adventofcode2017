package ja.adventofcode2017

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Day25 {

  case class SubRule(valueToWrite: Int, direction: Int, nextState: String)

  case class Rule(state: String, zeroRule: SubRule, oneRule: SubRule)

  def part1(input: String): Int = {
    val (startingState, diagnosticSteps, rules) = parse(input)
    var tape = Map[Int, Int]().withDefaultValue(0)

    var i = 0
    var nbrOnes = 0
    var state = startingState
    var pos = 0
    while (i < diagnosticSteps) {
      val rule = rules(state)

      val subRule = if (tape(pos) == 0) {
        rule.zeroRule
      } else {
        rule.oneRule
      }
      val valueToWrite = subRule.valueToWrite
      if (tape(pos) == 0 && valueToWrite == 1) {
        nbrOnes += 1
      } else if (tape(pos) == 1 && valueToWrite == 0) {
        nbrOnes -= 1
      }
      tape = tape.updated(pos, valueToWrite)
      pos += subRule.direction
      state = subRule.nextState
      i += 1
    }

    nbrOnes
  }

  private def parse(input: String) = {
    val lines = input.split("\n").map(_.trim())
    val startingState = "Begin in state ([A-Z]+)".r
      .findAllMatchIn(lines(0))
      .map { m =>
        m.subgroups.head
      }
      .toSeq
      .head

    val diagnosticSteps =
      "Perform a diagnostic checksum after ([0-9]+) steps.".r
        .findAllMatchIn(lines(1))
        .map { m =>
          m.subgroups.head
        }
        .toSeq
        .head
        .toInt

    var rules = Map[String, Rule]()

    var i = 3
    while (i < lines.size) {
      val state = "In state ([A-Z]+):".r
        .findAllMatchIn(lines(i))
        .map { m =>
          m.subgroups.head
        }
        .toSeq
        .head
      val valueToWrite = "Write the value ([0-9]+).".r
        .findAllMatchIn(lines(i + 2))
        .map { m =>
          m.subgroups.head
        }
        .toSeq
        .head
        .toInt
      val moveDirection = if (lines(i + 3).contains("to the right")) { 1 }
      else { -1 }
      val nextState = "Continue with state ([A-Z]+).".r
        .findAllMatchIn(lines(i + 4))
        .map { m =>
          m.subgroups.head
        }
        .toSeq
        .head
      val zeroRule = SubRule(valueToWrite, moveDirection, nextState)
      i += 4

      val valueToWrite1 = "Write the value ([0-9]+).".r
        .findAllMatchIn(lines(i + 2))
        .map { m =>
          m.subgroups.head
        }
        .toSeq
        .head
        .toInt
      val moveDirection1 = if (lines(i + 3).contains("to the right")) { 1 }
      else { -1 }
      val nextState1 = "Continue with state ([A-Z]+).".r
        .findAllMatchIn(lines(i + 4))
        .map { m =>
          m.subgroups.head
        }
        .toSeq
        .head
      val oneRule = SubRule(valueToWrite1, moveDirection1, nextState1)
      i += 6
      rules = rules.updated(state, Rule(state, zeroRule, oneRule))
    }
    (startingState, diagnosticSteps, rules)
  }

}
