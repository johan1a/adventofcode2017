package ja.adventofcode2017

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Day23 {

  def part1(input: String, n: Int = 5): Int = {
    val instructions = parse(input)
    var registers = Map[String, Long]().withDefaultValue(0L)
    var i = 0
    var sent = Seq[Long]()
    var received = Seq[Long]()
    val (_, _, _, _, mulCount) =
      execute(registers, instructions, i, sent, received)

    println(registers("h").toInt)
    mulCount
  }

  def part2(input: String, n: Int = 5): Int = {
    val b = 105700
    val c = 122700

    val primes = Array.fill(c+1)(true)
    var p = 2
    while (p * p <= c) {
      if (primes(p)) {
        var k = p * p
        while (k <= c) {
          primes(k) = false
          k = k + p
        }
      }
      p += 1
    }

    var count = 0
    var i = b
    while (i <= c) {
      if (!primes(i)) {
        count += 1
      }
      i += 17
    }

    count
  }

  private def execute(
      registers0: Map[String, Long],
      instructions: Seq[Seq[String]],
      i0: Int,
      sent0: Seq[Long],
      received0: Seq[Long]
  ): (Int, Map[String, Long], Seq[Long], Seq[Long], Int) = {
    var registers = registers0
    var sent = sent0
    var received = received0

    var mulCount = 0
    var i = i0
    while (i >= 0 && i < instructions.size) {
      val instruction = instructions(i.toInt)
      instruction match {
        case Seq("snd", a) =>
          sent = sent :+ getValue(registers, a)
        case Seq("set", a, b) =>
          registers = registers.updated(a, getValue(registers, b))
        case Seq("add", a, b) =>
          registers =
            registers.updated(a, registers(a) + getValue(registers, b))
        case Seq("sub", a, b) =>
          registers =
            registers.updated(a, registers(a) - getValue(registers, b))
        case Seq("mul", a, b) =>
          registers =
            registers.updated(a, registers(a) * getValue(registers, b))
          mulCount += 1
        case Seq("mod", a, b) =>
          registers =
            registers.updated(a, registers(a) % getValue(registers, b))
        case Seq("rcv", a) =>
          if (received.nonEmpty) {
            val next = received.head
            received = received.drop(1)
            registers = registers.updated(a, next)
          } else {
            return (i, registers, sent, received, mulCount)
          }
        case Seq("jgz", a, b) =>
          if (getValue(registers, a) > 0) {
            i += getValue(registers, b).toInt
            i -= 1
          }
        case Seq("jnz", a, b) =>
          if (getValue(registers, a) != 0) {
            i += getValue(registers, b).toInt
            i -= 1
          }
        case other =>
          println("error", other)
      }
      i += 1
    }

    (i, registers, sent, received, mulCount)
  }

  private def getValue(registers: Map[String, Long], x: String): Long = {
    if (x >= "a" && x <= "z") {
      registers(x)
    } else {
      x.toInt
    }
  }

  private def parse(input: String) = {
    input
      .split("\n")
      .map { line =>
        val split = line.trim().split(" ")
        split.toSeq
      }
      .toSeq
  }

}
