package ja.adventofcode2017

import scala.collection.mutable

object Day19 {

  def part1(input: String): String = {
    runMaze(input)._1
  }

  def part2(input: String): Int = {
    runMaze(input)._2
  }

  private def runMaze(input: String): (String, Int) = {
    val (grid, start) = parse(input)

    var letters = ""
    var reachedEnd = false
    var pos = start
    var dir = "down"
    var steps = 1
    while (!reachedEnd) {
      move(grid, pos, dir) match {
        case (nextPos, nextDir, nextReachedEnd) =>
          pos = nextPos
          dir = nextDir
          val cur = grid(pos._2)(pos._1)
          reachedEnd = nextReachedEnd || cur == ' '
      }
      if (!reachedEnd) {
        val cur = grid(pos._2)(pos._1)
        if (cur != ' ') {
          steps += 1
        }
        if (cur >= 'A' && cur <= 'Z') {
          letters += cur
        }
      }
    }

    (letters, steps)
  }

  private def printGrid(
      grid: Array[Array[Char]],
      pos: (Int, Int),
      dir: String
  ) = {
    println("=================")
    val dirs = Map(
      "up" -> "^",
      "down" -> "v",
      "right" -> ">",
      "left" -> "<"
    )
    (pos._2 - 8 until pos._2 + 8).foreach { y =>
      (pos._1 - 40 until pos._1 + 40).foreach { x =>
        if (y >= 0 && y < grid.size && x >= 0 && x < grid(0).size) {
          if (pos == (x, y)) {
            print(dirs(dir))
          } else {
            print(grid(y)(x))
          }
        }
      }
      println()
    }
  }

  private def move(
      grid: Array[Array[Char]],
      pos: (Int, Int),
      dir: String
  ): ((Int, Int), String, Boolean) = {
    val y = pos._2
    val x = pos._1
    val cur = grid(y)(x)
    if (y >= grid.size || y < 0 || x >= grid(0).size || x < 0 || cur == ' ') {
      return (pos, dir, true)
    }
    dir match {
      case "down" =>
        cur match {
          case ' ' =>
            (pos, dir, true)
          case '|' =>
            ((x, y + 1), dir, false)
          case '+' =>
            if (grid(y)(x + 1) != ' ') {
              ((x + 1, y), "right", false)
            } else if (grid(y)(x - 1) != ' ') {
              ((x - 1, y), "left", false)
            } else {
              (pos, dir, true)
            }
          case other =>
            ((x, y + 1), dir, false)
        }
      case "up" =>
        cur match {
          case ' ' =>
            (pos, dir, true)
          case '|' =>
            ((x, y - 1), dir, false)
          case '+' =>
            if (grid(y)(x + 1) != ' ') {
              ((x + 1, y), "right", false)
            } else if (grid(y)(x - 1) != ' ') {
              ((x - 1, y), "left", false)
            } else {
              (pos, dir, true)
            }
          case other =>
            ((x, y - 1), dir, false)
        }
      case "left" =>
        cur match {
          case ' ' =>
            (pos, dir, true)
          case '-' =>
            ((x - 1, y), dir, false)
          case '+' =>
            if (grid(y + 1)(x) != ' ') {
              ((x, y + 1), "down", false)
            } else if (grid(y - 1)(x) != ' ') {
              ((x, y - 1), "up", false)
            } else {
              (pos, dir, true)
            }
          case other =>
            ((x - 1, y), dir, false)
        }
      case "right" =>
        cur match {
          case ' ' =>
            (pos, dir, true)
          case '-' =>
            ((x + 1, y), "right", false)
          case '+' =>
            if (grid(y + 1)(x) != ' ') {
              ((x, y + 1), "down", false)
            } else if (grid(y - 1)(x) != ' ') {
              ((x, y - 1), "up", false)
            } else {
              (pos, dir, true)
            }
          case other =>
            ((x + 1, y), dir, false)
        }
    }
  }

  private def parse(input: String) = {
    var startX = -1
    val grid = input
      .split("\n")
      .map { line =>
        if (startX == -1) {
          startX = line.indexOf("|")
        }
        line.toArray
      }
      .toArray
    (grid, (startX, 0))
  }

}
