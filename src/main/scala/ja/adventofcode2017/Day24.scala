package ja.adventofcode2017

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Day24 {

  def part1(input: String): Int = {
    val components =
      input.split("\n").map(_.trim().split("/").map(_.toInt)).toSet
    findStrongest(components, 0)
  }

  def part2(input: String): Int = {
    val components =
      input.split("\n").map(_.trim().split("/").map(_.toInt)).toSet
    findLongest(components, 0)._2
  }

  private def findStrongest(components: Set[Array[Int]], prevPort: Int): Int = {
    if (components.isEmpty) {
      0
    } else {
      var possible = findComponents(components, prevPort)
      if (possible.isEmpty) {
        0
      } else {
        return possible.map { component =>
          val unusedPort = if (component.head == component.last) {
            prevPort
          } else {
            component.find(_ != prevPort).get
          }
          component.sum + findStrongest(components - component, unusedPort)
        }.max
      }
    }
  }

  private def findLongest(
      components: Set[Array[Int]],
      prevPort: Int
  ): (Int, Int) = {
    if (components.isEmpty) {
      (0, 0)
    } else {
      var possible = findComponents(components, prevPort)
      if (possible.isEmpty) {
        (0, 0)
      } else {
        return possible
          .map { component =>
            val unusedPort = if (component.head == component.last) {
              prevPort
            } else {
              component.find(_ != prevPort).get
            }
            val (length, strength) =
              findLongest(components - component, unusedPort)
            (length + 1, strength + component.sum)
          }.max
      }
    }
  }

  private def findComponents(components: Set[Array[Int]], port: Int) = {
    components.filter(c => c.head == port || c.last == port)

  }

}
