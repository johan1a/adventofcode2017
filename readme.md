
# Advent of code 2017

## Run all tests

```
sbt test
```

## Run tests for single day

```
sbt
testOnly ja.adventofcode2017.Day14*
```
