name := "advencofcode2017-2"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % "test"

Test / fork := true
Test / javaOptions += "-Xmx6G"
